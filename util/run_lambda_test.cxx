//lambda
#include "HHLambdaTool/HHLambdaTool.h"

//std/stl
#include <iostream>
#include <cstdlib>
#include <string>
#include <cstring> // strcmp
using namespace std;

//ROOT


void help()
{
    cout << "-----------------------------------------------------------------" << endl;
    cout << " run_lambda_test" << endl;
    cout << "-----------------------------------------------------------------" << endl;
}

int main(int argc, char** argv)
{
    string input_file = "";

    for(int i = 1; i < argc; i++)
    {
        //if(argv[i] == "-h") { help(); return 0; }
        if (strcmp(argv[i], "-h") == 0) { help(); return 0; }
        else if(strcmp(argv[i], "-i") == 0) input_file = argv[++i];
        else
        {
            cout << "run_lambda_test    Unknown command line argument '" << argv[i] << "', exiting" << endl;
            return 1;
        }
    } // i

    hh::HHLambdaTool lambda_tool;

    try
    {
        lambda_tool.load();
    }
    catch(std::exception& e)
    {
        cout << "run_lambda_test    FAIL: " << e.what() << endl;
    }

    float w = lambda_tool.get_weight(-2, 455);
    cout << "run_lambda_test    Weight for lambda = 2 at mhh = 455 = " << w << endl;

    auto all_weights = lambda_tool.get_weights(455);
    cout << "run_lambda_test    Weights for mhh = 455:" << endl;
    for(auto lambda : all_weights)
    {
        cout << "run_lambda_test     > " << lambda.first << " = " << lambda.second << endl;
    }

    return 0;
}
