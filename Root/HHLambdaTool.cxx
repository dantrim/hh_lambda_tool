//hh
#include "HHLambdaTool/HHLambdaTool.h"

//ROOT
#include "TFile.h"
#include "TH1.h"
#include "TH1F.h"
#include "TList.h"
#include "TKey.h"

//Asg
#include "PathResolver/PathResolver.h"

//std/stl
#include <iostream>
#include <sstream>
using namespace std;


namespace hh
{

HHLambdaTool::HHLambdaTool()
{
}

HHLambdaTool& HHLambdaTool::load(std::string filename)
{
    stringstream filename_app;
    filename_app << "HHLambdaTool/" << filename;
    string full_name  = PathResolverFindCalibFile(filename_app.str());

    if(full_name == "")
    {
        stringstream err;
        err << "HHLambdaTool::load    ERROR Unable to find requested reweighting file (" << filename << ")";
        throw std::runtime_error(err.str());
    }

    cout << "HHLambdaTool::load    Loading reweighting file: " << filename << endl;

    TFile* rfile = TFile::Open(full_name.c_str());
    if(rfile->IsZombie())
    {
        stringstream err;
        err << "HHLambdaTool::load    ERROR Reweighting file opened as Zombie" << endl;
        throw std::runtime_error(err.str());
    }

    //rfile->ls();

    retrieve_histograms(rfile);

    return *this;
}

void HHLambdaTool::retrieve_histograms(TFile* rfile)
{

 //  TFile *file = new TFile("hsimple.root");
 //  TObject *obj;
 //  TKey *key;
 //  TIter next( file->GetListOfKeys());
 //  while ((key = (TKey *) next())) {
 //     obj = file->Get(key->GetName()); // copy object to memory
 //     // do something with obj
 //     //       printf(" found object:%s\n",key->GetName());
 //     //          }
    

    TObject* obj;
    TKey* key;
    TIter next( rfile->GetListOfKeys() );
    while( (key = (TKey*)next()))
    {
        obj = rfile->Get(key->GetName());
        if(obj->InheritsFrom("TH1"))
        {
            //cout << "HHLambdaTool::retrieve_histograms    Found histogram " << obj->GetName() << endl;
            retrieve_histogram((TH1F*)obj);
        }
    } // key

    cout << "HHLambdaTool::retrieve_histograms    Found " << m_lambda_histo_map.size() << " histograms in " << rfile->GetName() << endl;

}

void HHLambdaTool::retrieve_histogram(TH1F* histo)
{
    string histo_name = histo->GetName();
    string find_str = "mHH_lambda";
    size_t loc = histo_name.find(find_str); //find(histo_name.begin(), histo_name.end(), find_str);

    if(loc == std::string::npos)
    {
        // ignore those that do not have expected format
        return;
    }

    string lambda_str = histo_name.substr(find_str.length());

    int lambda_val = -99;
    try
    {
        lambda_val = std::atoi(lambda_str.c_str());
    }
    catch(std::exception& e)
    {
        stringstream err;
        err << "HHLambdaTool::retrieve_histogram    ERROR Failed to retrieve lambda value from histogram named " << histo_name;
        throw std::runtime_error(err.str());
    }

    if(m_lambda_histo_map.count(lambda_val))
    {
        stringstream err;
        err << "HHLambdaTool::retrieve_histogram    ERROR Encountered multiple lambda histograms for lambda value " << lambda_val;
        throw std::runtime_error(err.str());
    }

    m_lambda_histo_map[lambda_val] = histo;
    load_histogram(lambda_val);
}

void HHLambdaTool::load_histogram(int lambda)
{
    TH1F* hist = nullptr;
    if(m_lambda_histo_map.count(lambda) == 0)
    {
        stringstream err;
        err << "HHLambdaTool::get_weight    ERROR Requesting weight for unexpected lambda value for which there are no histograms (requestd lambda = " << lambda << ")";
        throw std::runtime_error(err.str());
    }

    hist = m_lambda_histo_map.at(lambda);

    std::vector<float> edge_vec;
    std::vector<float> w_vec;
    std::vector<float> w_err_vec;

    for(unsigned int ibin = 0; ibin < hist->GetNbinsX() + 1; ibin++)
    {
        float upper_edge = hist->GetBinLowEdge(ibin) + hist->GetBinWidth(ibin);
        float w = hist->GetBinContent(ibin);
        float e = hist->GetBinError(ibin);
        edge_vec.push_back(upper_edge);
        w_vec.push_back(w);
        w_err_vec.push_back(e);
    } // ibin

    // overflow ??
    edge_vec.push_back(1e9);
    w_vec.push_back(hist->GetBinContent(hist->GetNbinsX()+1));
    w_err_vec.push_back(hist->GetBinError(hist->GetNbinsX()+1));

    m_nbins_map[lambda] = edge_vec.size();

    m_histo_upper_edge_map[lambda] = edge_vec;
    m_weight_map[lambda] = w_vec;
    m_weight_err_map[lambda] = w_err_vec;

}

float HHLambdaTool::get_weight(int lambda, float mhh)
{
    int nbins = m_nbins_map.at(lambda);
    for(unsigned int ibin = 0; ibin < nbins; ibin++)
    {
        if( mhh < m_histo_upper_edge_map.at(lambda).at(ibin) )
        {
            return m_weight_map.at(lambda).at(ibin);
        }
    } // ibin
    return m_weight_map.at(lambda).back();
}
std::map<int, float> HHLambdaTool::get_weights(float mhh)
{
    std::map<int, float> out;

    for(auto lambda : m_lambda_histo_map)
    {
        out[lambda.first] = this->get_weight(lambda.first, mhh);
    } // lambda

    return out;
}



} // namespace hh
