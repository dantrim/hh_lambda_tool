#ifndef HHLAMBDA_TOOL_H
#define HHLAMBDA_TOOL_H

//std/stl
#include <string>
#include <vector>
#include <map>

//ROOT
class TH1F;
class TFile;

namespace hh
{

class HHLambdaTool
{
    public :
        HHLambdaTool();
        virtual ~HHLambdaTool(){};

        // Load the reweighting histogram file
        // throws exception if failure mode encountered
        HHLambdaTool& load(std::string reweight_filename = "mHH_combinations_bin10ratios.root");

        // retrieve the k-factor to reweight from SM case to 'lambda' case
        float get_weight(int lambda, float truth_mhh /*GeV*/);

        // retrieve a map of k-factors for a given input truth level mhh
        // map is keyed by the lambda to which the reweighting has been performed
        std::map<int, float> get_weights(float truth_mhh /*GeV*/);

    private :


        std::string m_reweight_filename;
        std::map<int, TH1F*> m_lambda_histo_map;

        std::map<int, int> m_nbins_map;
        std::map<int, std::vector<float> > m_histo_upper_edge_map;
        std::map<int, std::vector<float> > m_weight_map;
        std::map<int, std::vector<float> > m_weight_err_map;

        void load_histogram(int lambda);
        void retrieve_histograms(TFile* reweight_file = nullptr);
        void retrieve_histogram(TH1F* histo = nullptr);
        
        
        

}; // class HHLambdaTool

} // namespace hh

#endif
