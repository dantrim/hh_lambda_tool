# HHLambdaTool

Tool to implement Higgs self-coupling constant variations based on truth-level di-higgs invariant mass.

## Installation

```bash
cd <analysis-dir>
mkdir source/
cd source/
lsetup "asetup AnalysisBase,21.2.55,here"
git clone https://:@gitlab.cern.ch:8443/dantrim/hh_lambda_tool.git HHLambdaTool
cd ..
mkdir build
cd build/
cmake ../source
make
source x86*/setup.sh
```

At which point the test executable `run_lambda_test` should be callable and should give output resembling:

```bash
HHLambdaTool::load    Loading reweighting file: mHH_combinations_bin10ratios.root
HHLambdaTool::retrieve_histograms    Found 41 histograms in ...
run_lambda_test    Weight for lambda = 2 at mhh = 455 = 3.95061
run_lambda_test    Weights for mhh = 455:
run_lambda_test     > -20 = 66.2318
run_lambda_test     > -19 = 60.7669
...
```

## Using the tool in your analysis

After compiling this package, there you will have the library `HHLambdaToolLib`. Linking to this and including
the relevant headers,etc in your analysis package's *CMakeLists.txt* file is all you need to do in order to use
the tool:

```cmake
atlas_depends_on_subdirs(
    ...
    HHLambdaTool
)

atlas_add_library(
    ...
    LINK_LIBRARIES ... HHLambdaToolLib ...
)
```

And then in your analysis code, initialize the tool and retrieve the "k-factor" ("lambda_weight" in the code snippet below) to scale your input
*hh* signal to one corresponding to the varied trilinear coupling "lambda_new":

```c++
hh::HHLambdaTool lambda_tool;
lambda_tool.load(/*reweighting-file*/);
...
// calculate/obtain truth-level dihiggs invariant mass and store in variable "truth_mhh"
float lambda_weight = lambda_tool.get_weight(/*int*/ lambda_new, /*float*/ truth_mhh);

```

The ```load()``` method takes as input an `std::string` which is the name of the reweighting file you wish to use.
You just need to provide the name of the file and that file must be located in the *data/* sub-directory of the
HHLambdaTool package at compile time. By default (i.e. providing no arguments to this method, as in the snippet
above) it will load the SM reweighting file.

The tool also has a method to return a map, `std::map<int, float>`, object which stores the "k-factor" value associated
with the provided "truth_mhh" for all possible trilinear coupling values that were found in the input
reweighting file provided to the ```load(...)``` method of the tool:

```c++
...
auto all_lambda_weights = lambda_tool.get_weights(/*float*/ truth_mhh);
float weight_lambda_SM = all_lambda_weights.at(1); // SM corresponds to lambda_hhh = 1
float weight_lambda_5 = all_lambda_weights.at(5); // get weight for lambda_hhh = 5
...
```
